var gulp = require('gulp');
const path = require('path');
const ROOT_DIR = process.cwd();
const webpackStream = require('webpack-stream');
const webpackConfig = require(`${ROOT_DIR}/webpack.config.js`);
const webpack = require('webpack');
const BUILD_DIR = webpackConfig.output.path;

gulp.task("server", () => {
    const liteServer = require('lite-server');

    process.chdir(path.resolve(BUILD_DIR));
    liteServer.server();
});

gulp.task('default', () => gulp.src('src/start.ts')
    .pipe(webpackStream(webpackConfig, webpack))
    .on('error', function handleError() {
        console.log('ERROR');
    })
    .pipe(gulp.dest(BUILD_DIR)));