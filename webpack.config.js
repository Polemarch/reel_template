const fs = require('fs');
const path = require('path');
const ROOT_DIR = process.cwd();
const nodeModules = `${ROOT_DIR}/node_modules`;

module.exports = {
    context: ROOT_DIR,
    entry : [`${nodeModules}/pixi.js/dist/pixi.js`, `${nodeModules}/gsap/src/uncompressed/TweenMax.js`, './src/start.ts'],
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'bin'),
        filename: `output.js`
    },
    devtool: 'none' ,
    resolve: {
        extensions: ['.js', '.ts'],
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                use: 'source-map-loader'

            },
            {
                test: /\.ts$/,
                use: `awesome-typescript-loader${''}`,
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                use: "dot-loader"
            }
        ]
    }
};