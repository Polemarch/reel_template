export const GameConstants = {
    SYMBOLS: {
        SYMBOL_HEIGHT: 525,
        SYMBOL_IDS: [0,1,2],
        VISIBLE_SYMBOLS_COUNT: 3
    },
    REELS: {
        TIME_UP: 0.5,
        SPIN_TIME: 3,
        TIME_STOP: 0.2,
        REEL_SPEED: 0.2 // per 1 switch reel
    },
    REJECT: {
        NOT_TERMINABLE: 'NOT_TERMINABLE'
    },
    RESOURCES: {
        INITIAL_LOAD: [
            'assets/coin.png',
            'assets/reelBack.png',
            'assets/spin_button.png',
            'assets/symbol_0.png',
            'assets/symbol_1.png',
            'assets/symbol_2.png'
        ],
        REEL_BACK: 'assets/reelBack.png',
        SPIN_BUTTON: 'assets/spin_button.png',
        COIN: 'assets/coin.png',
    }
}