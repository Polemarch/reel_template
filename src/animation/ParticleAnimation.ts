import { Container, loader } from 'pixi.js';
import { Emitter } from 'pixi-particles';
import { GameConstants } from '../GameConstants';
import { ResourceLoader } from '../core/loader/ResourceLoader';
import { EventDispatcher } from '../core/dispatcher/EventDispatcher';
import { EventType } from '../core/dispatcher/EventType';

export class ParticleAnimation extends Container {

    private _emitter: Emitter;
    private _elapsed: number;
    private _stopped: boolean;

    constructor() {
        super();
        this.visible = false;
        this.setListeners();
        this.createEmitter();
    }

    private setListeners(): void {
        EventDispatcher.instance.addObserver(EventType.SHOW_ANIMATION, this.showAnimation.bind(this));
        EventDispatcher.instance.addObserver(EventType.HIDE_ANIMATION, this.hideAnimation.bind(this));
    }

    public createEmitter(): void {
        this._emitter = new Emitter(this, ResourceLoader.instance.getTexture(GameConstants.RESOURCES.COIN), this._emytterConfig);
    }

    public showAnimation(): void {
        this._elapsed = Date.now();
        this._stopped = false;
        this.visible = true;
        this._emitter.emit = true;
        this._emitter.resetPositionTracking();
        this.update();
    }

    public hideAnimation(): void {
        this._stopped = true;
        this._emitter.cleanup();
        this.visible = false;
    }

    private update(){
        if (this._stopped) {
            return;
        }
        requestAnimationFrame(this.update.bind(this));

        const now = Date.now();

        this._emitter.update((now - this._elapsed) * 0.001);
        this._elapsed = now;
    };

    private _emytterConfig = {
        alpha: {
            list: [
                {
                    value: 1,
                    time: 0
                },
                {
                    value: 0.6,
                    time: 1
                }
            ],
            isStepped: false
        },
        scale: {
            list: [
                {
                    value: 0.7,
                    time: 0
                },
                {
                    value: 0.3,
                    time: 1
                }
            ],
            isStepped: false
        },
        color: {
            list: [
                {
                    value: "fb1010",
                    time: 0
                },
                {
                    value: "f5b830",
                    time: 1
                }
            ],
            isStepped: false
        },
        speed: {
            list: [
                {
                    value: 500,
                    time: 0
                },
                {
                    value: 200,
                    time: 1
                }
            ],
            isStepped: false
        },
        startRotation: {
            min: 0,
            max: 360
        },
        rotationSpeed: {
            min: 100,
            max: 2
        },
        lifetime: {
            min: 0.5,
            max: 1
        },
        frequency: 0.08,
        spawnChance: 1,
        particlesPerWave: 10,
        emitterLifetime: 1.3,
        maxParticles: 1000,
        pos: {
            x: 10,
            y: 0
        },
        addAtBack: false,
        spawnType: "circle",
        spawnCircle: {
            x: 0,
            y: 0,
            r: 15
        }
    };
}