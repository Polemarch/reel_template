import { ReelView } from "./ReelView";
import { SymbolTape } from "../symbols/SymbolTape";
import { GameConstants } from "../GameConstants";
import { Container } from "pixi.js";
import { ReelSpinController } from "./ReelSpinController";
import { EventDispatcher } from "../core/dispatcher/EventDispatcher";
import { EventType } from "../core/dispatcher/EventType";

export class ReelController {
    private _reelView: ReelView;
    private _reelSpinController: ReelSpinController;
    private _tapes: SymbolTape[];

    public init(parentContainer: Container): void {
        this._tapes = [];
        this._reelSpinController = new ReelSpinController();

        this._reelView = new ReelView();
        this._reelView.init();
        parentContainer.addChild(this._reelView);

        const tapeHeight: number = this.tapeHeight;

        for (let i: number = 0; i < 3; i++) {
            const tape: SymbolTape = new SymbolTape();
            tape.initTape(this.tapeIds);
            this._tapes.push(tape);
            tape.y = tapeHeight * i;
            this._reelView.symbolContainer.addChild(tape);
        }
        this._reelView.symbolContainer.y = - tapeHeight;

        this.setListeners();
    }

    public resize(width: number, height: number): void {
        this._reelView.height = height;
        this._reelView.scale.x = this._reelView.scale.y;

        if (this._reelView.scale.x > 1 || this._reelView.scale.y > 1) {
            this._reelView.scale.x = this._reelView.scale.y = 1;
        }

        this._reelView.x = (width - this._reelView.width) / 2;
        this._reelView.y = (height - this._reelView.height) / 2;
    }

    private setListeners(): void {
        EventDispatcher.instance.addObserver(EventType.START_SPIN, this.onStartSpin.bind(this));
        EventDispatcher.instance.addObserver(EventType.STOP_SPIN, this.onStopSpin.bind(this));
    }

    private onStartSpin(resolve?: Function): void {
        this._reelSpinController.start(this._tapes)
            .then(() => resolve());
    }

    private onStopSpin(resolve?: Function): void {
        this._reelSpinController.stop()
            .then(() => resolve());
    }

    private get tapeHeight(): number {
        return GameConstants.SYMBOLS.SYMBOL_HEIGHT * GameConstants.SYMBOLS.VISIBLE_SYMBOLS_COUNT;
    }

    private get tapeIds(): number[] {
        const result = Array(GameConstants.SYMBOLS.VISIBLE_SYMBOLS_COUNT).fill(0);
        const allIds = GameConstants.SYMBOLS.SYMBOL_IDS;
        for (let i: number = 0; i < result.length; i++) {
            result[i] = allIds[Math.floor(Math.random() * allIds.length)];
        }

        return result;
    }
}