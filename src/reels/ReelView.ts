import { Container, Sprite, Graphics } from "pixi.js";
import { SpinButton } from "../ui/SpinButton";
import { ResourceLoader } from "../core/loader/ResourceLoader";
import { GameConstants } from "../GameConstants";
import { ParticleAnimation } from "../animation/ParticleAnimation";

export class ReelView extends Container{
    private _symbolContainer: Container;
    private _spinButton: SpinButton;
    private _background: Sprite;
    private _particleAnimation: ParticleAnimation;

    public init(): void {
        this._spinButton = new SpinButton();
        this._background = ResourceLoader.instance.getResource(GameConstants.RESOURCES.REEL_BACK);
        this._symbolContainer = new Container();
        this._particleAnimation = new ParticleAnimation();
        this._particleAnimation.x = 250;
        this._particleAnimation.y = 500;

        this._spinButton.x = this._background.width + 150;
        this._spinButton.y = this._background.height - this._spinButton.height - 10;

        this._symbolContainer.x = 30;

        this.addChild(this._symbolContainer);
        this.addChild(this._spinButton);
        this.addChild(this._background);

        this.addChild(this._particleAnimation);

        this.drawMask();
    }

    public drawMask(): void {
        const mask: Graphics = new Graphics();
        mask.beginFill(0x0, 0);
        mask.drawRect(0,0,this._background.width, this._background.height);
        mask.endFill();

        this.addChild(mask);
        this.symbolContainer.mask = mask;
    }

    public get symbolContainer(): Container {
        return this._symbolContainer;
    }
}