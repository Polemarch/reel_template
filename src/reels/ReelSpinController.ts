import { SymbolTape } from "../symbols/SymbolTape";
import { GameConstants } from "../GameConstants";

export class ReelSpinController {
    private _tapes: SymbolTape[];
    private _tween: TweenLite;
    private _stopped: boolean;
    private _stopResolve: Function;

    public start(tapes:SymbolTape[]): Promise<any> {
        this._tapes = tapes;
        this._stopped = false;

        return new Promise((resolve) => {
            TweenLite.to(this._tapes, GameConstants.REELS.TIME_UP, {onUpdate: () => {
                    this._tapes.forEach((tape: SymbolTape) => {
                        tape.y -= 2;
                    })
                },
                onComplete: () => {
                    this.spin();
                    resolve();
                }
            })
        })
    }

    public spin(): void {
        TweenLite.to(this._tapes[0], GameConstants.REELS.REEL_SPEED, {y:this._tapes[0].y + this._tapes[0].height, ease:Linear.easeNone})
        TweenLite.to(this._tapes[1], GameConstants.REELS.REEL_SPEED, {y:this._tapes[1].y + this._tapes[1].height, ease:Linear.easeNone})
        TweenLite.to(this._tapes[2], GameConstants.REELS.REEL_SPEED, {y:this._tapes[2].y + this._tapes[2].height, ease:Linear.easeNone, onComplete:() => {
            this.onCompleteLoop();
        }})
    }

    private onCompleteLoop(): void {
        this._tapes[2].y = 0;
        this._tapes.unshift(this._tapes.pop());
        if (this._stopped) {
            this.stopAnimation();
        } else {
            this.spin();
        }
        
    }

    public stop(): Promise<any> {
        this._stopped = true;
        return new Promise((resolve) => {
            this._stopResolve = resolve;
        })
    }

    private stopAnimation(): void {
        TweenLite.to(this._tapes, GameConstants.REELS.TIME_STOP, {onUpdate: () => {
                this._tapes.forEach((tape: SymbolTape) => {
                    tape.y += 4;
                })
            },
            onComplete: () => {
                TweenLite.to(this._tapes, GameConstants.REELS.TIME_STOP, {onUpdate: () => {
                    this._tapes.forEach((tape: SymbolTape) => {
                        tape.y -= 4;
                    })
                },
                onComplete: this._stopResolve
            })
            }
        })
    }

    private clearTweben(): void {
        if (this._tween) {
            this._tween.kill();
            this._tween = null;
        }
    }
}