import { ActionManager } from "../actions/ActionManager";
import { Container } from "pixi.js";
import { ReelController } from "../reels/ReelController";

export class GameController {
    private _actionManager: ActionManager;
    private _mainContainer: Container;
    private _reelsController: ReelController;

    public init(mainContainer: Container): void {
        this._mainContainer = mainContainer;

        this._actionManager = new ActionManager();
        this._actionManager.initActions();

        this._reelsController = new ReelController();
    }

    public startGame(): void {
        this._reelsController.init(this._mainContainer);
        this._actionManager.run();
    }

    public onResize(width: number, height: number): void {
        this._reelsController.resize(width, height);
    }
}