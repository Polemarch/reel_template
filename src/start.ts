import { GameController } from "./controllers/GameController";
import { Application } from "pixi.js";
import { ResourceLoader } from "./core/loader/ResourceLoader";
import { GameConstants } from "./GameConstants";

export class Main {
    private _gameController: GameController;
    private _mainApplication: Application;

    public initGame(): void {
        this._mainApplication = new Application(window.innerWidth, window.innerHeight, {backgroundColor : 0x1099bb});
        document.body.appendChild(this._mainApplication.view);
        this._mainApplication.renderer.plugins.interaction.autoPreventDefault = true;
 
        this._gameController = new GameController();
        this._gameController.init(this._mainApplication.stage);

        window.addEventListener("resize", () => {
            this.onResize(window.innerWidth, window.innerHeight);
        });

        ResourceLoader.instance.loadResources(GameConstants.RESOURCES.INITIAL_LOAD)
            .then(() => {this.onResurcesLoaded()});
    }

    protected onResurcesLoaded(): void {
       this._gameController.startGame();
       this.onResize(window.innerWidth, window.innerHeight);
    }

    public onResize(clientWidth: number, clientHeight: number): void {
        this._mainApplication.renderer.resize(clientWidth, clientHeight);
        this._gameController.onResize(clientWidth, clientHeight);
    }
}

var main = new Main();
main.initGame();