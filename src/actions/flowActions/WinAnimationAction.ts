import { BaseAction } from "../BaseAction";
import { EventDispatcher } from "../../core/dispatcher/EventDispatcher";
import { EventType } from "../../core/dispatcher/EventType";

export class WinAnimationAction extends BaseAction {
    public onExecute(): Promise<any> {
        return new Promise((resolve) => {
            EventDispatcher.instance.dispatch(EventType.SHOW_ANIMATION);
            setTimeout(() => {
                EventDispatcher.instance.dispatch(EventType.HIDE_ANIMATION);
                resolve();
            }, 2000)
        })
    }
}