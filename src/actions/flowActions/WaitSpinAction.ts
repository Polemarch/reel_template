import { BaseAction } from "../BaseAction";

export class WaitSpinAction extends BaseAction {
    public onExecute(): Promise<any> {
        return new Promise((resolve) => {
            setTimeout(() => {
                console.log('waited !!')
                resolve();
            }, 1000)
        })
    }
}