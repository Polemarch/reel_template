import { BaseAction } from "../BaseAction";
import { EventDispatcher } from "../../core/dispatcher/EventDispatcher";
import { EventType } from "../../core/dispatcher/EventType";

export class WaitClickAction extends BaseAction {

    private _mainResolve: Function;

    constructor() {
        super();
        EventDispatcher.instance.addObserver(EventType.CLICK, () => {
            if (this._mainResolve) {
                console.log('CLICK!!!');
                this._mainResolve();
                this.readyToFinish();
            }
        })
    }

    public onExecute(): Promise<any> {
        return new Promise((resolve) => {
            this._mainResolve = resolve;
        })
    }

    private readyToFinish(): void {
        this._mainResolve = null;
    }
}