import { BaseAction } from "../BaseAction"
import { EventDispatcher } from "../../core/dispatcher/EventDispatcher";
import { EventType } from "../../core/dispatcher/EventType";

export class MakeSpinAction extends BaseAction {
    public onExecute(): Promise<any> {
        return new Promise((resolve) => {
            EventDispatcher.instance.dispatch(EventType.START_SPIN, resolve);
        })
    }
}