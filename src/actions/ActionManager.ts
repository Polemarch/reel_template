import { IAction } from "./IAction";
import {MakeSpinAction} from './flowActions/MakeSpinAction'
import { WaitSpinAction } from "./flowActions/WaitSpinAction";
import { StopReelAction } from "./flowActions/StopReelAction";
import { WinAnimationAction } from "./flowActions/WinAnimationAction";
import { WaitClickAction } from "./flowActions/WaitClickAction";

export class ActionManager {
    private _actions: IAction[];
    private _currentPosition: number;
    private _isRunning: boolean;

    public initActions(): void {
        this._actions = [];
        this._isRunning = false;

        this._actions.push(new WaitClickAction());
        this._actions.push(new MakeSpinAction());
        this._actions.push(new WaitSpinAction());
        this._actions.push(new StopReelAction());
        this._actions.push(new WinAnimationAction());
    }

    public run(): void {
        if (this._isRunning) {
            return;
        }
        this._isRunning = true;

        this._currentPosition = 0;
        this.runNextAction();
    }

    private runNextAction(): Promise<any> {
        return this._actions[this.stepPosition].execute()
            .then(this.runNextAction.bind(this));
    }

    private get stepPosition(): number {
        const currentStep = this._currentPosition;
        this._currentPosition++;
        if (this._currentPosition >= this._actions.length) {
            this._currentPosition = 0;
        }
        return currentStep;
    }
}