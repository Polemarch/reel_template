export interface IAction {
    execute(): Promise<any>;
    terminate(): Promise<any>;
}