import { IAction } from "./IAction";
import { GameConstants } from "../GameConstants";

export class BaseAction implements IAction {
    protected get isTerminable(): boolean {
        return false;
    }

    protected get guard(): boolean {
        return true;
    }

    protected onExecute(): Promise<any> {
        return Promise.resolve();
    }

    protected onTerminate(): Promise<any> {
        return Promise.resolve();
    }

    public execute(): Promise<any> {
        if (this.guard) {
            return this.onExecute();
        }

        return Promise.resolve();
    }

    public terminate(): Promise<any> {
        if (this.isTerminable) {
            return this.onTerminate();
        }

        return Promise.reject(GameConstants.REJECT.NOT_TERMINABLE);
    }

}