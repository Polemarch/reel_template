import { Sprite, Texture } from "pixi.js";

export class ResourceLoader {
    private static _instance: ResourceLoader;
    private _loader: PIXI.loaders.Loader;

    public static get instance(): ResourceLoader {
        if (!ResourceLoader._instance) {
            ResourceLoader._instance = new ResourceLoader();
        }
        return ResourceLoader._instance;
    }

    private constructor() {
        this._loader = new PIXI.loaders.Loader;
    }



    public loadResources(resources: string[]): Promise<any> {
        resources.forEach((res: string) => {
            this._loader.add(res);
        });

        return new Promise((resolve) => {
            this._loader.load(resolve);
        });
    }

    public getTexture(name: string): Texture {
        if (!PIXI.utils.TextureCache[name]) {
            throw new Error('Loader -> No such texture');
        }
        return PIXI.utils.TextureCache[name];
    }

    public getResource(name: string): Sprite {
        if (!PIXI.utils.TextureCache[name]) {
            throw new Error('Loader -> No such texture');
        }
        return new Sprite(PIXI.utils.TextureCache[name]);
    }
}