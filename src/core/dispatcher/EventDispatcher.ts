export class EventDispatcher {
    private static _instance: EventDispatcher;

    private _observers: any;

    private constructor() {
        this._observers = {};
    }

    public dispatch(type: string, ...args): void {
        if (!this._observers[type]) {
            return;
        }

        this._observers[type].forEach((observer) => {
            observer(...args);
        });
    }

    public addObserver(type: string, observer: Function): void {
        if (!this._observers[type]) {
            this._observers[type] = [];
        }
        this._observers[type].push(observer);
    }

    public removeObserver(type: string, observer: Function): void {
        if (!this._observers[type]) {
            return;
        }

        this._observers[type].splice(this._observers[type].indexOf(observer), 1);
    }
    

    public static get instance(): EventDispatcher {
        if (!EventDispatcher._instance) {
            EventDispatcher._instance = new EventDispatcher();
        }
        return EventDispatcher._instance;
    }
}