export const EventType = {
    CLICK: 'click',
    START_SPIN: 'start_spin',
    STOP_SPIN: 'stop_spin',
    SHOW_ANIMATION: 'show_animation',
    HIDE_ANIMATION: 'hide_animation',
}