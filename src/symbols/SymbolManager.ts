import { Sprite } from "pixi.js";
import { ResourceLoader } from "../core/loader/ResourceLoader";

export class SymbolManager {
    public static getSymbolById(id: number): Sprite {
        return ResourceLoader.instance.getResource('assets/symbol_'+ id.toString()+'.png');
    }
}