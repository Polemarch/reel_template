import { Container, DisplayObject, Sprite } from "pixi.js";
import { SymbolManager } from "./SymbolManager";
import { GameConstants } from "../GameConstants";

export class SymbolTape extends Container {
    private _symbols: DisplayObject[];

    constructor() {
        super();
        this._symbols = [];
    }

    public initTape(ids: number[]): void {
        this.clearTape();
        for (let i: number = 0; i < ids.length; i++) {
            const symbol: Sprite = SymbolManager.getSymbolById(ids[i]);
            symbol.y = GameConstants.SYMBOLS.SYMBOL_HEIGHT * i;
            this.addChild(symbol);
        }
    }

    public clearTape(): void {
        this.removeChildren();
    }
}