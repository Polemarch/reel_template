import { Container, Sprite } from "pixi.js";
import { ResourceLoader } from "../core/loader/ResourceLoader";
import { GameConstants } from "../GameConstants";
import { EventDispatcher } from "../core/dispatcher/EventDispatcher"
import { EventType } from "../core/dispatcher/EventType";

export class SpinButton extends Container{
    private _image: Sprite;

    constructor() {
        super();
        this._image = ResourceLoader.instance.getResource(GameConstants.RESOURCES.SPIN_BUTTON);
        this._image.x = -this._image.width / 2;
        this._image.y = -this._image.height / 2;
        this.addChild(this._image);

       this.setListeners();
    }

    private setListeners(): void {
        this.interactive = true;
        this.addListener('pointerover', this.onOver.bind(this))
        this.addListener('pointerout', this.onOut.bind(this))
        this.addListener('pointerdown', this.onDown.bind(this))
        this.addListener('pointerup', this.onUp.bind(this))
    }

    private onOver(): void {
        this.scale.x = this.scale.y = 0.9;
    }

    private onOut(): void {
        this.scale.x = this.scale.y = 1;
    }

    private onDown(): void {
        this.scale.x = this.scale.y = 0.6;
    }

    private onUp(): void {
        this.scale.x = this.scale.y = 1;
        EventDispatcher.instance.dispatch(EventType.CLICK)
    }

    public destroy(): void {
        this.removeChildren();
        this.removeAllListeners();
    }


}